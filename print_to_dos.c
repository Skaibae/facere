/*
 * This file is part of Facere.
 *
 * Facere is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by Free Software Foundation.
 *
 * Facere is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 3
 * of the GNU General Public License for more details.
 *
 * You should have received a copy of version 3 of the GNU General Public
 * License along with Facere.  If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * To contact the author of Facere, send an email to
 * <seblavine@outlook.com>.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "link.h"

void print_to_dos() {
// Prints all to-dos with their number.
  const char *homedir = getenv("HOME");
  char *filename = malloc((strlen(homedir) + 1) * sizeof(char));
  strcpy(filename, homedir);
  filename = realloc(filename, strlen(filename) + strlen("/.facere"));
  filename = strcat(filename, "/.facere");
  
  FILE *fp = fopen(filename, "r");
  if (fp == NULL) {
    puts("File \".facere\" does not exist. Have you written to it yet?");
    exit(0);
  }

  char *current_string = NULL;
  size_t cs_size = 0;
  for (int i = 1; getline(&current_string, &cs_size, fp) != -1; i++)
    printf("%i. %s", i, current_string);
//  putchar('\n');
  free(current_string);
}
