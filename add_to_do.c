/*
 * This file is part of Facere.
 *
 * Facere is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by Free Software Foundation.
 *
 * Facere is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 3
 * of the GNU General Public License for more details.
 *
 * You should have received a copy of version 3 of the GNU General Public
 * License along with Facere.  If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * To contact the author of Facere, send an email to
 * <seblavine@outlook.com>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "link.h"
#include "defs.h"

void add_to_do(int pos_in_argv, int argc, char **argv) {
  const char *homedir = getenv("HOME");
  char *filename = malloc((strlen(homedir) + 1) * sizeof(char));
  strcpy(filename, homedir);
  filename = realloc(filename, strlen(filename) + strlen("/.facere"));
  filename = strcat(filename, "/.facere"); // filename = location of .facere, in home dir
  
  FILE *fp = fopen(filename, "a"); // opens fp for appending to file

  char *new_to_do = malloc(sizeof(char));
  unsigned ntd_size = 1;
  // concatenates all arguments after 'add/create/new' to one string
  for (int i = pos_in_argv + 1; i < argc; i++) {
    ntd_size += strlen(*(argv + i)) + 1; // + 1 is for space
    new_to_do = realloc(new_to_do, ntd_size * sizeof(char));
    strcat(new_to_do, *(argv + i));
    if (i < argc - 1)
      strcat(new_to_do, " ");
  }
  
  #define stringsize ntd_size - 2
  if (fwrite(new_to_do, sizeof(char), stringsize, fp) == stringsize)
    printf("New to-do \"%s\" added.\n", new_to_do);
  else
    printf("New to-do \"%s\" failed to be added.\n", new_to_do);
  fputc('\n', fp);
}
