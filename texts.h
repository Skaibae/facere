/*
 * This file is part of Facere.
 *
 * Facere is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by Free Software Foundation.
 *
 * Facere is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 3
 * of the GNU General Public License for more details.
 *
 * You should have received a copy of version 3 of the GNU General Public
 * License along with Facere.  If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * To contact the author of Facere, send an email to
 * <seblavine@outlook.com>.
 */

const char *license_info = "Facere - a simple command-line to-do program\nCopyright (C) 2018 Sebastian LaVine\n\nThis program is free software: you can redistribute it and/or modify\nit under the terms of version 3 of the GNU General Public License as\npublished by the Free Software Foundation.\nThis program is distributed in the hope that it will be useful,\nbut WITHOUT ANY WARRANTY; without even the implied warranty of\nMERCHANTABILITY or FITNESS OF A PARTICULAR PURPOSE. See version 3\nof the GNU General Public License for more details.\n\nYou should have received a copy of version 3 of the GNU General Public\nLicense along with this program. If not, see\n<https://www.gnu.org/licenses/>.\nTo contact the author of this program, send an email to\n<seblavine@outlook.com>.\n\nFor help using Facere, type 'facere help'.";

const char *help_info = "Facere - a simple command-line to-do program\nCommands:\nhelp - Displays this help.\ninfo - displays licensing info.\nversion - prints the version of Facere being used.\nnew/create/add - Creates a new to-do.\nconquer/delete/remove - Removes a to-do.\nwipeclean - Removes all to-dos.\nlist/print - Prints all to-dos, with their numerical IDs.";
