#ifndef DEFS_H
#define DEFS_H

size_t filelen(FILE *fp) {
  rewind(fp);
  size_t len = 0;
  while (fgetc(fp) != EOF)
    len++;
  rewind(fp);
  return len - 1;
}

#endif
